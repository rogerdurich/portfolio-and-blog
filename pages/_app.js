import Head from "next/head";
import NavBar from "../components/NavBar/NavBar";
import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>{`Roger's Portfolio and Blog`}</title>
        <meta name="description" content="A place to document all the amazing things my team and I do." />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="w-screen">
        <NavBar />
        <Component {...pageProps} />
      </main>
    </>
  );
}

export default MyApp;
