import { gql } from "graphql-request";
import { WriteData } from "../../graphql/_writer";

const mutation = gql`
  mutation CreateDraft {
    createPost(
      data: {
        title: $title
        date: $date
        content: $content
        tags: $tags
        coverImage: { connect: { id: $imageId } }
        authors: { connect: { id: $authorId } }
      }
    ) {
      id
    }
  }
`;

module.exports = async (req, res) => {
  try {
    const headers = req.headers.authorization
    const body = JSON.stringify(req.body)
    res.send(await WriteData(mutation, headers, body));
  } catch ({ status = 500, message, ...rest }) {
    res.status(status).json({ status, message });
  }
};
