import { gql } from 'graphql-request'
import { GetData } from '../../graphql/_reader'

module.exports = async (req, res) => {
  try {
    const query = gql`${req.body.query}`
    const headers = req.headers.authorization
    res.send(await GetData(query, headers))    
  } catch ({status = 500, message, ...rest}) {
    res.status(status).json({status, message})
  }  
}