import Waterpipe, {options} from "./Waterpipe"

export default function Hero() {
  return (
    <section className="mt-2 max-h-fit max-w-full">
      <Waterpipe options={options} />
    </section>
  )
}