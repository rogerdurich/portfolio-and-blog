import Search from "./Search";

export default function NavBar() {
  return (
    <nav className="container mx-auto grid grid-cols-3 mt-4">
      <ul className="flex">
        <li>
          <h1 className="text-3xl">{`Roger's Recollections`}</h1>
        </li>
      </ul>
      <ul className="flex justify-center mt-2">
        <li className="px-6">
          <a href="#" className="font-display max-w-sm text-xl leading-tight">
            <span className="link link-underline link-underline-black text-indigo-900">
              {" "}
              Blog Posts{" "}
            </span>
          </a>
        </li>
        <li className="px-6">
          <a href="#" className="font-display max-w-sm text-xl leading-tight">
            <span className="link link-underline link-underline-black text-indigo-900">
              {" "}
              Portfolio{" "}
            </span>
          </a>
        </li>
      </ul>
      <ul className="flex justify-end">
        <li>
          <Search />
        </li>
      </ul>
    </nav>
  );
}
