require("dotenv").config();
import { GraphQLClient } from "graphql-request";



export async function WriteData(mutation, headers, body) {
    const endpoint = process.env.ENDPOINT

    const graphqlClient = new GraphQLClient(endpoint, {
        headers: {
            authorization: headers
        },
        body: body
    });

    let data = {}

    try {
        data = await graphqlClient.request(mutation)
    } catch (error) {
        return error        
    }
    return data;
}