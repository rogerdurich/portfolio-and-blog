require("dotenv").config();
import { GraphQLClient } from 'graphql-request'

export async function GetData(query, headers) {
    const endpoint = process.env.ENDPOINT

    const graphqlClient = new GraphQLClient(endpoint, {
        headers: {
            authorization: headers
        },
    })

    let data = {}

    try {
        data = await graphqlClient.request(query);            
    } catch (error) {
        return error;
    }
    return data;
}